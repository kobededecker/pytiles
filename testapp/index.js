var map = L.map("map").setView([51, 4], 10)
var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var wms = L.tileLayer.wms("http://localhost:5000/wms", {
    layers: "dem",
    format:"image/png"
}).addTo(map)